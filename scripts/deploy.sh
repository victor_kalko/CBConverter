#!/usr/bin/env bash

#mvn clean package

echo 'Copy files'

#csp -i ~/.ssh/id_rsa \
#  target/CBConverter-0.0.1-SNAPSHOT.jar \
#  admin@84.201.180.73:/home/admin/

echo 'Restart server'

ssh -tt -i ~/.ssh/id_rsa admin@84.201.180.73 << EOF

pgrep java | xargs kill -9
nohup java -jar CBConverter-0.0.1-SNAPSHOT.jar > log.txt &

EOF

echo 'Bye'