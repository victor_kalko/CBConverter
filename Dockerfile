FROM alpine

RUN apk add git \
	&& apk add yarn \
	&& git clone https://github.com/viktorKolco/CBConverter.git \
	&& cd CBConverter \
	&& yarn
WORKDIR ./CBConverter
	
CMD yarn start

EXPOSE 3000
	
