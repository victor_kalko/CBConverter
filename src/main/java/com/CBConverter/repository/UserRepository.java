package com.CBConverter.repository;

import converter.cbfeedback.domain.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByName(String name);

    UserEntity findByActivationCode(String code);
}
