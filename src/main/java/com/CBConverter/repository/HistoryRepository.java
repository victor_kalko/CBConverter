package com.CBConverter.repository;

import com.CBConverter.domain.entities.HistoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface HistoryRepository extends CrudRepository<HistoryEntity, Integer> {

    Optional<HistoryEntity> findTopByOrderByIDDesc();

    List<HistoryEntity> findAllByUSERIDOrderByDATEDesc(Long userId);

    List<HistoryEntity> findAllByOrderByDATEDesc();

    List<HistoryEntity> findAllByDATELessThanEqualAndDATEGreaterThanEqual(LocalDateTime endDate, LocalDateTime startDate);

    void deleteByUSERID(Long userId);

    HistoryEntity getByID(int id);
}
