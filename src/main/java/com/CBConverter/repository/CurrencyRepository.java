package com.CBConverter.repository;

import com.CBConverter.domain.entities.CurrencyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRepository extends CrudRepository<CurrencyEntity, Long> {

    Optional<CurrencyEntity> findByCharCode(String charCode);
}
