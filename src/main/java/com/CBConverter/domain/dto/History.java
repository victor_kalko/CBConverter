package com.CBConverter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class History {

    private Integer ID;

    private BigDecimal AMOUNT_RECEIVED;

    private String ORIGINAL_CURRENCY;

    private String TARGET_CURRENCY;

    private BigDecimal TOTAL_AMOUNT;

    private String DATE;

    private Long USERID;

}
