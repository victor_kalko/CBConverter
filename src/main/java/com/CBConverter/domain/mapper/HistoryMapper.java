package com.CBConverter.domain.mapper;

import com.CBConverter.domain.dto.History;
import com.CBConverter.domain.entities.HistoryEntity;
import org.mapstruct.Mapper;

@Mapper
public interface HistoryMapper extends DirectMapper<HistoryEntity, History>{

    History map(HistoryMapper source);
}
