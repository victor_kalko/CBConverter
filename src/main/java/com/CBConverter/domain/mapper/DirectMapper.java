package com.CBConverter.domain.mapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;

public interface DirectMapper<FROM, TO> {
    DateTimeFormatter target = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");

    TO map(FROM source);

    List<TO> map(Collection<FROM> sourceList);

    default String dateResolver(LocalDateTime date) {
        return date.format(target);
    }
}
