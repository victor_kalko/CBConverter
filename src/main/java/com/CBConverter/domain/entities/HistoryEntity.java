package com.CBConverter.domain.entities;

import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Builder
@Table(name = "history")
@FieldNameConstants
@AllArgsConstructor
@NoArgsConstructor
public class HistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ID;

    @Column(name = "amount_received", nullable = false)
    private BigDecimal AMOUNT_RECEIVED;

    @Column(name = "original_currency", nullable = false)
    private String ORIGINAL_CURRENCY;

    @Column(name = "target_currency", nullable = false)
    private String TARGET_CURRENCY;

    @Column(name = "total_amount", nullable = false)
    private BigDecimal TOTAL_AMOUNT;

    @Column(name = "date", nullable = false)
    private LocalDateTime DATE;

    @Column(name = "user_id", nullable = false)
    private Long USERID;

    public HistoryEntity(String originalCurrency, String targetCurrency, BigDecimal amountReceived, BigDecimal totalAmount, LocalDateTime date, Long userId) {
        this.ORIGINAL_CURRENCY = originalCurrency;
        this.TARGET_CURRENCY = targetCurrency;
        this.AMOUNT_RECEIVED = amountReceived;
        this.TOTAL_AMOUNT = totalAmount;
        this.DATE = date;
        this.USERID = userId;
    }

    @Override
    public String toString() {
        return "History{" +
                "ID=" + ID +
                ", AMOUNT_RECEIVED=" + AMOUNT_RECEIVED +
                ", ORIGINAL_CURRENCY='" + ORIGINAL_CURRENCY + '\'' +
                ", TARGET_CURRENCY='" + TARGET_CURRENCY + '\'' +
                ", TOTAL_AMOUNT=" + TOTAL_AMOUNT +
                ", DATE=" + DATE +
                ", USER_ID=" + USERID +
                '}';
    }
}
