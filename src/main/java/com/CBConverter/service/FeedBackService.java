package com.CBConverter.service;

public interface FeedBackService {

    /**
     * <p>Добавить отзыв.</p>
     * @param comment  комментарий, который привязывается к Admin (пока что)
     **/
    String addComment(String comment);
}