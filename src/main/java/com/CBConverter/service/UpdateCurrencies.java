package com.CBConverter.service;

import com.CBConverter.domain.entities.CurrencyEntity;
import com.CBConverter.repository.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateCurrencies {
    private final CurrencyRepository currencyRepository;
    private final ResponseService responseService;

    @Scheduled(fixedRate = 600000)
    private void updateCurrencies() {
        log.info("=== Обновление курсов валют по шедулеру ===");
        List<CurrencyEntity> list = responseService.getCurrenciesInfo();
        if (!list.isEmpty()) {
            currencyRepository.saveAll(list);
        }
        else {
            log.error("=== Курсы валют не были обновлены ===");
        }
    }
}

