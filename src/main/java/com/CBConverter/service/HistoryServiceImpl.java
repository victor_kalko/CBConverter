package com.CBConverter.service;

import com.CBConverter.domain.dto.History;
import com.CBConverter.domain.entities.HistoryEntity;
import com.CBConverter.domain.mapper.HistoryMapper;
import com.CBConverter.repository.HistoryRepository;
import converter.cbfeedback.domain.UserRole;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class HistoryServiceImpl implements HistoryService {
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final HistoryRepository historyRepository;
    private final ConverterService converterService;
    private final UserService userService;
    private final HistoryMapper mapper;
    private static HistoryEntity temp = new HistoryEntity("USD (Доллар США)",
            "RUB (Российский рубль)",
            BigDecimal.valueOf(1),
            BigDecimal.valueOf(30),
            LocalDateTime.now(),
            1L);

    @Override
    public HistoryEntity getById(int id) {
        return historyRepository.getByID(id);
    }

    @Override
    public Map<String, Object> getLastConvert() {
        BigDecimal total = temp.getTOTAL_AMOUNT();
        BigDecimal amount = temp.getAMOUNT_RECEIVED();
        String original = temp.getORIGINAL_CURRENCY();
        String target = temp.getTARGET_CURRENCY();
        Map<String, Object> model = new HashMap<>();
        model.put("originalCurrency", original);
        model.put("targetCurrency", target);
        model.put("originalChar", original.substring(0, 3));
        model.put("targetChar", target.substring(0, 3));
        model.put("totalAmount", total);
        model.put("amountReceived", amount);
        return model;
    }

    @Override
    public void getRotated(){
        String tmp = temp.getORIGINAL_CURRENCY();
        temp.setORIGINAL_CURRENCY(temp.getTARGET_CURRENCY());
        temp.setTARGET_CURRENCY(tmp);
    }

    @Override
    public String addHistory(String originalCurrency, String targetCurrency, BigDecimal amountReceived) {
        String error = null;
        try {
            if (amountReceived == null) throw new IllegalArgumentException("Вы должны что-то ввести");
            if (amountReceived.compareTo(BigDecimal.valueOf(0)) <= 0)
                throw new IllegalArgumentException("Введенное значение должно быть положительным");
            LocalDateTime date = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
            temp = new HistoryEntity(
                    converterService.toDescription(originalCurrency),
                    converterService.toDescription(targetCurrency),
                    amountReceived,
                    converterService.convert(originalCurrency, targetCurrency, amountReceived),
                    date,
                    userService.getCurrentUser().getId());
            historyRepository.save(temp);
            log.info("Произведена конвертация : '{}'", temp);
        } catch (IllegalArgumentException e) {
            log.info("Конвертация не была произведена. ");
            error = format("ОШИБКА: '%s'", e.getMessage());
        }
        return error;
    }

    @Override
    public List<History> getByDate(String date) {
        List<HistoryEntity> histories;

        if (date != null && !date.equals("")) {
            LocalDate targetDate = LocalDate.parse(date, formatter);
            histories = historyRepository.findAllByDATELessThanEqualAndDATEGreaterThanEqual(
                    targetDate.plusDays(1).atStartOfDay(), targetDate.atStartOfDay());
        } else {
            histories = historyRepository.findAllByOrderByDATEDesc();
        }
        return mapper.map(histories);
    }

    @Override
    @Transactional
    public void deleteAllByCurrentUser() {
        Long userId = userService.getCurrentUser().getId();
        historyRepository.deleteByUSERID(userId);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        historyRepository.deleteById(id);
    }

    @Override
    public List<History> findAllByCurrentUser() {
        return mapper.map(userService.getCurrentUser().getRole() == UserRole.ADMIN
                ? historyRepository.findAllByOrderByDATEDesc()
                : historyRepository.findAllByUSERIDOrderByDATEDesc(userService.getCurrentUser().getId()));
    }

    @Override
    @Transactional
    public void deleteAll() {
        historyRepository.deleteAll();
    }
}
