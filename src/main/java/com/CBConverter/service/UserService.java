package com.CBConverter.service;

import converter.cbfeedback.domain.UserEntity;

public interface UserService {

    /**
     * <p>Добавить нового пользователя.</p>
     **/
    String addUser(String name, String password, String email, String passwordAgain);

    /**
     * <p>Активировать пользователя.</p>
     * @param code код активации
     **/
    void activateUser(String code);

    /**
     * <p>Получить текущего пользователя.</p>
     * @return текущий пользователь
     **/
    UserEntity getCurrentUser();
}
