package com.CBConverter.service;

import com.CBConverter.domain.dto.History;
import com.CBConverter.domain.entities.HistoryEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public interface HistoryService {


    /**
     * <p>Возвращает конвертацию по конкретному идентитфикатору.</p>
     * @param id уникальный идентификатор сущности истории
     **/
    HistoryEntity getById(int id);

    /**
     * <p>Получить последнюю конвертацию из БД.</p>
     * @return последняя конвертация.
     **/
    Map<String, Object> getLastConvert();

    /**
     * <p>Поменять местами original/target.</p>
     **/
    void getRotated();

    /**
     * <p>Добавить в историю новую конвертацию.</p>
     * @param originalCurrency оригинальный курс, с которого производится конвертиирование
     * @param targetCurrency   курс, в который необходимо конвертировать
     * @param amountReceived   количество единиц для конвертации в валюте originalCurrency
     * @return null в случае успеха. Ошибка в случае неудачи.
     **/
    String addHistory(String originalCurrency, String targetCurrency, BigDecimal amountReceived);

    /**
     * <p>Получить несколько конвертаций по определенной дате.</p>
     * @param date дата, на которую необзодимо получить конвертации
     * @return конвертации
     **/
    List<History> getByDate(String date);

    /**
     * <p>Удаляет все конвертации по текущему пользователю.</p>
     **/
    void deleteAllByCurrentUser();

    /**
     * <p>Удаляет конвертацию по конкретному идентитфикатору.</p>
     * @param id идентификатор для удаления
     **/
    void deleteById(int id);

    /**
     * <p>Получить все конвертации по конкретному пользователю.</p>
     * @return все конвертации пользователя
     **/
    List<History> findAllByCurrentUser();

    /**
     * <p>Удаляет ВСЕ конвертации из БД.</p>
     * Доступ только из-под роли 'ADMIN'
     **/
    void deleteAll();
}
