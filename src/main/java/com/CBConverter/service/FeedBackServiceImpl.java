package com.CBConverter.service;

import converter.cbfeedback.feign.FeedbackFeign;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class FeedBackServiceImpl implements FeedBackService{

    private final FeedbackFeign feedbackFeign;
    private final UserService userService;

    @Override
    public String addComment(String comment) {
        String error = null;
        try {
            if (comment.length() > 200) throw new IllegalArgumentException("Ваш отзыв слишком длинный.");
            if (comment.isBlank()) throw new IllegalArgumentException("Поле не может быть пустым.");
            feedbackFeign.addComment(userService.getCurrentUser(), comment);
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
            error = format("ОШИБКА: '%s'", e.getMessage());
        }
        return error;
    }
}
