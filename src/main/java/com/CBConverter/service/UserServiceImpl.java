package com.CBConverter.service;

import com.CBConverter.repository.UserRepository;
import converter.cbfeedback.domain.UserEntity;
import converter.cbfeedback.domain.UserRole;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final MailSender mailSender;
    private final static String activationMessage = "Пожалуйста, пройдите по ссылке для активации аккаунта:\n" +
            "http://%s/activate/%s";
    private final static String EMAIL_PATTERN = "(.*)@(.*).(.*)";
    private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

    @Value("${hostname}")
    private String hostname;

    @Override
    public String addUser(String name, String password, String email, String passwordAgain) {
        UserEntity user = new UserEntity(name, password, email);
        String error = null;
        UserEntity userFromDB = userRepository.findByName(name);
        log.info("Запрос на регистрацию: name : '{}', email: '{}'", name, email);
        try {
            if (name.isBlank()) throw new IllegalArgumentException("Имя пользователя не может быть пустым.");
            if (name.length() > 50) throw new IllegalArgumentException("Имя пользователя должно быть меньше 50 символов.");
            if (userFromDB != null) throw new IllegalArgumentException("Пользователь с таким именем уже существует.");
            if (!password.equals(passwordAgain)) throw new IllegalArgumentException("Пароли не совпадают.");
            if (!isValid(email)) throw new IllegalArgumentException("Email не валиден.");

            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setActive(false);
            user.setName(user.getName());
            user.setRole(UserRole.USER);
            user.setActivationCode(UUID.randomUUID().toString());
            userRepository.save(user);

            mailSender.send(user.getEmail(), "ActivationCode", format(activationMessage, hostname, user.getActivationCode()));
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
            error = format("ОШИБКА: '%s'", e.getMessage());
        }
        return error;
    }

    @Override
    public void activateUser(String code) {
        UserEntity user = userRepository.findByActivationCode(code);

        if (user == null) return;

        user.setActivationCode(null);
        user.setActive(true);
        userRepository.save(user);
        log.debug("Пользователь '{}' был активирован и обновлен: '{}'", user.getId(), user.toString());
    }

    @Override
    public UserEntity getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (null == auth) {
            throw new IllegalStateException("Не нашел пользователя!!");
        }

        Object obj = auth.getPrincipal();
        String username;

        if (obj instanceof UserDetails) {
            username = ((UserDetails) obj).getUsername();
        } else {
            username = obj.toString();
        }

        return userRepository.findByName(username);
    }

    private static boolean isValid(String password) {
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

}
