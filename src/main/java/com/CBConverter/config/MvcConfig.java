package com.CBConverter.config;

import com.CBConverter.domain.mapper.HistoryMapper;
import com.CBConverter.domain.mapper.HistoryMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Bean
    public HistoryMapper historyMapper() {
        return new HistoryMapperImpl();
    }
}