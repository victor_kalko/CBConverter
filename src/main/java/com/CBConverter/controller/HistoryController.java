package com.CBConverter.controller;

import com.CBConverter.domain.dto.History;
import com.CBConverter.service.HistoryService;
import com.CBConverter.service.UserService;
import converter.cbfeedback.domain.UserEntity;
import converter.cbfeedback.domain.UserRole;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Controller
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService historyService;
    private final UserService userService;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @GetMapping("/history")
    public String history(@RequestParam(required = false, defaultValue = "") String date,
                          Map<String, Object> model, HttpServletRequest request, Model frontError) {
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        UserEntity user = userService.getCurrentUser();

        if (map != null) {
            frontError.addAttribute("frontError", map.get("error"));
        }

        List<History> histories = date == null
                ? historyService.findAllByCurrentUser()
                : historyService.getByDate(date).stream()
                .filter(history -> history.getUSERID().equals(user.getId()))
                .collect(Collectors.toList());

        model.put("HISTORIES", histories);
        model.put("isADMIN", userService.getCurrentUser().getRole() == UserRole.ADMIN);
        model.put("isHistoryEmpty", histories.isEmpty());
        assert date != null;
        if (!date.isBlank()) {
            model.put("dateToFront", LocalDate.parse(date, formatter));
        } else {
            model.put("dateToFront", LocalDate.now());
        }
        return "history";
    }

    @PostMapping("deleteAll")
    public String deleteAll(RedirectAttributes redirectAttributes) {
        String message;
        if (userService.getCurrentUser().getRole() == UserRole.ADMIN) {
            historyService.deleteAll();
            message = "Конвертации ВСЕХ пользователей успешно удалены.";
        } else {
            historyService.deleteAllByCurrentUser();
            message = "Конвертации успешно удалены.";
        }
        redirectAttributes.addFlashAttribute("error", message);
        return "redirect:/history";
    }

    @PostMapping("delete")
    public String delete(@RequestParam("id") Integer id, RedirectAttributes redirectAttributes) {
        LocalDateTime date = historyService.getById(id).getDATE();
        historyService.deleteById(id);
        redirectAttributes.addFlashAttribute("error", format("Конвертация от '%s' успешно удалена.", date));
        return "redirect:/history";
    }
}
