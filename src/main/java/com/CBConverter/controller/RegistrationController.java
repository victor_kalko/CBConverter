package com.CBConverter.controller;

import com.CBConverter.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @GetMapping("/registration")
    public String registrationError(HttpServletRequest request, Model frontError) {
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        if (map != null) {
            frontError.addAttribute("frontError", map.get("error"));
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@RequestParam("name") String name, @RequestParam("password") String password,
                          @RequestParam("passwordAgain") String passwordAgain, @RequestParam("email") String email,
                          RedirectAttributes redirectAttributes) {
        String error = userService.addUser(name, password, email, passwordAgain);
        if (error != null) {
            redirectAttributes.addFlashAttribute("error", error);
            return "redirect:/registration";
        }
        return "activate";
    }

    @GetMapping("/activate/{code}")
    public String activate(@PathVariable String code) {
        userService.activateUser(code);
        return "login";
    }

    @GetMapping("/activate")
    public String activate() {
        return "activate";
    }
}
