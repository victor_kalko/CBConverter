package com.CBConverter.controller;

import com.CBConverter.service.FeedBackService;
import converter.cbfeedback.feign.FeedbackFeign;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class FeedBackController {

    private final FeedbackFeign feedbackFeign;
    private final FeedBackService feedBackService;

    @GetMapping("/feedback")
    public String feedBack(HttpServletRequest request, Model model) {

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        if (map != null) {
            model.addAttribute("frontError", map.get("error"));
        }

        model.addAttribute("comments", feedbackFeign.getComments());

        return "feedback";
    }

    @PostMapping("/feedback")
    public String addComment(@RequestParam("comment") String comment, RedirectAttributes redirectAttributes) {

        String error = feedBackService.addComment(comment);

        if (error == null) {
            redirectAttributes.addFlashAttribute("error", "Комментарий успешно добавлен.");
            redirectAttributes.addFlashAttribute("isError", false);
        } else {
            redirectAttributes.addFlashAttribute("error", error);
            redirectAttributes.addFlashAttribute("isError", true);
        }
        return "redirect:/feedback";
    }

    @PostMapping("/feedback/delete")
    public String delete(@RequestParam("id") Integer id, RedirectAttributes redirectAttributes) {
        feedbackFeign.deleteById(id);

        redirectAttributes.addFlashAttribute("error", "Отзыв успешно удален.");
        return "redirect:/feedback";
    }
}
