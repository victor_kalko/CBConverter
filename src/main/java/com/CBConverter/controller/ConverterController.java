package com.CBConverter.controller;

import com.CBConverter.service.HistoryService;
import com.CBConverter.service.ResponseService;
import com.CBConverter.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ConverterController {

    private final ResponseService responseService;
    private final HistoryService historyService;
    private final UserService userService;

    @GetMapping("/converter")
    public String converter(Map<String, Object> model,
                            HttpServletRequest request, Model frontError) {
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        if (map != null) {
            frontError.addAttribute("frontError", map.get("error"));
            frontError.addAttribute("isError", map.get("isError"));
        }

        model.putAll(historyService.getLastConvert());
        return "converter";
    }

    @PostMapping("/converter")
    public String add(@RequestParam String postOriginalCurrency, @RequestParam String postTargetCurrency,
                      @RequestParam BigDecimal postAmountReceived, RedirectAttributes redirectAttributes) {
        String error = historyService.addHistory(postOriginalCurrency, postTargetCurrency, postAmountReceived);
        if (error != null) {
            redirectAttributes.addFlashAttribute("error",  error);
            redirectAttributes.addFlashAttribute("isError", true);
            return "redirect:/converter";
        }
        return "redirect:/converter";
    }

    @GetMapping("refresh")
    public String refresh(RedirectAttributes redirectAttributes) {
        log.info("Обновление курсов валют по требованию пользователя");
        responseService.getCurrenciesInfo();
        redirectAttributes.addFlashAttribute("error",  "Курсы валют успешно обновлены.");
        redirectAttributes.addFlashAttribute("isError",  false);
        return "redirect:/converter";
    }

    @GetMapping("rotate")
    public String rotate() {
        log.info("Запрос на смену местами original/target");
        historyService.getRotated();
        return "redirect:/converter";
    }

    @GetMapping
    public String main(Model userName) {
        userName.addAttribute("userName", userService.getCurrentUser().getName());
        return "main";
    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }
}
