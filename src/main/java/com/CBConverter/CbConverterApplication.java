package com.CBConverter;

import converter.cbfeedback.feign.FeedbackFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableKafka
@EnableScheduling
@SpringBootApplication
@ComponentScan(basePackages = {"com", "converter.cbfeedback.*"})
@EntityScan(value = {"com", "converter.cbfeedback.*"})
@EnableFeignClients(clients = {
        FeedbackFeign.class
})
//@EnableEurekaClient
public class CbConverterApplication {

    public static void main(String[] args) {
        SpringApplication.run(CbConverterApplication.class, args);
    }
}
