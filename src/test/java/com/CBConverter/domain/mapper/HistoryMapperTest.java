package com.CBConverter.domain.mapper;

import com.CBConverter.domain.dto.History;
import com.CBConverter.domain.entities.HistoryEntity;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@DisplayName("Collecting state mapper")
class HistoryMapperTest {

    private final EasyRandom rnd = new EasyRandom();
    private final HistoryMapper testingMapper = new HistoryMapperImpl();
    private final static DateTimeFormatter target = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");

    @Test
    @DisplayName("History mapping")
    void map() {
        HistoryEntity source = rnd.nextObject(HistoryEntity.class);

        History result = testingMapper.map(source);

        assertEquals(source.getAMOUNT_RECEIVED(), result.getAMOUNT_RECEIVED(), "Incorrect mapping of 'AMOUNT_RECEIVED' field");
        assertEquals(source.getTOTAL_AMOUNT(), result.getTOTAL_AMOUNT(), "Incorrect mapping of 'TOTAL_AMOUNT' field");
        assertEquals(source.getID(), result.getID(), "Incorrect mapping of 'ID' field");
        assertEquals(source.getORIGINAL_CURRENCY(), result.getORIGINAL_CURRENCY(), "Incorrect mapping of 'ORIGINAL_CURRENCY' field");
        assertEquals(dateResolver(source.getDATE()), result.getDATE(), "Incorrect mapping of 'DATE' field");
        assertEquals(source.getUSERID(), result.getUSERID(), "Incorrect mapping of 'USERID' field");
        assertEquals(source.getTARGET_CURRENCY(), result.getTARGET_CURRENCY(), "Incorrect mapping of 'TARGET_CURRENCY' field");
    }

    private String dateResolver(LocalDateTime date) {
        return date.format(target);
    }

}
